# Envoie SMS
Ce projet décrit les étapes à mettre en place pour identifer un client et lui envoyer un sms. Vous trouverez plus de détails sur le fichier **MyDigitAssist_API_SMS**.

## Dialplan
* /etc/asterisk/extensions_extra.d/svi-mydigitassist.conf
```
chown root:www-data /etc/asterisk/extensions_extra.d/svi-mydigitassist.conf
chmod 660 /etc/asterisk/extensions_extra.d/svi-mydigitassist.conf
```

## AGI
* /var/lib/asterisk/agi-bin/mydigitassist.py
```
chown asterisk:asterisk /var/lib/asterisk/agi-bin/mydigitassist.py
```

## LOG
* Créer le fichier des logs
```
touch /var/log/xivo-opteven-ws-request-agi.log
chown asterisk:asterisk /var/log/xivo-opteven-ws-request-agi.log
```
* Logrotate
```
touch /etc/logrotate.d/xivo-opteven-ws-request-agi
```

## SDA
* Il faut finalement mettre **svi-mydigitassist-sms** dans la sous de prétretement de la SDA concerné.





Fiche Wiki : https://wiki.avencall.com/index.php/Customers/Opteven
~                                                                  
