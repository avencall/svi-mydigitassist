#!/usr/bin/python
# -*- coding: UTF-8 -*-
# Copyright (C) 2020  Avencall
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along
# with this program; if not, write to the Free Software Foundation, Inc.,

import argparse
import logging
import sys
import traceback
import requests

from xivo.agi import AGI

#
# User configuration
# -------------

DEBUG_MODE = True # False
LOGFILE = '/var/log/xivo-opteven-ws-request-agi.log'

#
# Factory configuration
# -------------

#WS_URL = 'appdigitassistdev.opteven.lan:8080'
WS_URL = 'my-digitassist.com'
#WS_URL = 'my-digitassist.com/homol/'
WS_HEADERS = {
        'Accept': 'application/json',
        'Content-Type': 'application/json',
        }
WS_URL_TIMEOUT = 2


#Initialisation

agi = AGI()

logger = logging.getLogger()

class Syslogger(object):

    def write(self, data):
        global logger
        logger.error(data)


def init_logging(debug_mode):
    if debug_mode:
        logger.setLevel(logging.DEBUG)
    else:
        logger.setLevel(logging.INFO)

    logfilehandler = logging.FileHandler(LOGFILE)

    formatter = logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s')
    logfilehandler.setFormatter(formatter)
    logger.addHandler(logfilehandler)

    syslogger = Syslogger()
    sys.stderr = syslogger


def get_ws_info(appelant, appelee):
    payload = {'number': appelant, 'service': appelee}
    logger.debug("Using param: service_number = %s AND phone_number = %s" % (appelee, appelant))
    auth_ws_url = "https://%s/api/v1/communicator/sms" % WS_URL
    check_access_request = None
    ip = WS_URL
    sms_status=0
    try:
        check_access_request = requests.get(auth_ws_url,
                                            params=payload,
                                            headers=WS_HEADERS,
                                            timeout=30)

    except requests.exceptions.ConnectionError:
        logger.warning("Server " + ip + " refused connection")

    except ValueError:
        logger.error("No JSON encoded response from " + ip)

    except requests.exceptions.Timeout:
        logger.error("Timeout when connecting to the following server " + ip)

    except:
        logger.error("Unknown problem during connection to the following server: " + ip)
        exc_type, exc_value, exc_traceback = sys.exc_info()
        logger.debug(repr(traceback.format_exception(exc_type, exc_value,
                                          exc_traceback)))


    if check_access_request != None:
        if check_access_request.status_code != requests.codes.ok:
            if check_access_request.status_code != 401:
                logger.warning("Unexpected status code " + str(check_access_request.status_code) + " onserver " + ip)
                logger.error(check_access_request.json())
            else:
                logger.error("Authentication issue, Wrong user or password on server " + ip)
                return None
        else:
            logger.info("Request is OK. Response = %s" % check_access_request.status_code)
            sms_status= check_access_request.status_code
            return sms_status
    else:
        logger.error("Unknown problem during connection to the following server: " + ip)
        return None


def set_dialplan_variable(sms_status):
    logger.info("Setting dialplan variable using dict data extracted from WS ")
    if sms_status != None:
        logger.info("sms_status_is OK")
        agi.set_variable("sms_status", "SMS_OK")
    else: 
        logger.info("sms_status_is NOK")
        agi.set_variable("sms_status", "SMS_NOK")

def main():
    init_logging(DEBUG_MODE)
    try:
        if len(sys.argv) < 2:
            logger.error("wrong number of arguments\nUsage: opteven_ws_request.py CALLER_ID XIVO_DSTNUM")
            sys.exit(1)

        sms_status = get_ws_info(sys.argv[1], sys.argv[2])

        set_dialplan_variable(sms_status)

    except Exception:
        exc_type, exc_value, exc_traceback = sys.exc_info()
        logger.error(repr(traceback.format_exception(exc_type, exc_value,
                                                       exc_traceback)))
        sys.exit(1)

    sys.exit(0)

if __name__ == '__main__':
    main()
